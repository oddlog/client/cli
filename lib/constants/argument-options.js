import {LIST as LEVELS} from "@oddlog/levels";

/*===================================================== Exports  =====================================================*/

/*------------------------------------------------------ Filter ------------------------------------------------------*/

export const filter = {

  commands: {
    l: {
      alias: "level",
      describe: "Filter logs of this severity and above.",
      string: true,
      nargs: 1
    },
    d: {
      alias: "date",
      describe: "Filter logs by date and/or time. The accepted format is based on the ISO format " +
          "YYYY-MM-DDTHH:mm:ss.sTZD. Parts of this can be omitted. The T can be replaced with whitespace as well. If " +
          "no TZD is provided, local timezone will be used. Valid examples include '16-1-1' (anything on the 1st " +
          "January of **16), '1 12Z' (anything on the 1st of any month at [12:00,13:00) in UTC) and 'T12:0+2:4' " +
          "(anything on any day at [12:00,13:00) in timezone +02:04).",
      string: true,
      nargs: 1
    },
    name: {
      alias: ["logger", "ns"],
      describe: "Filter logs by logger name. The `ns-matcher` package is used for matching; See " +
          "https://www.npmjs.com/package/ns-matcher#examples . This allows advanced and multiple (in-order) glob " +
          "patterns.",
      array: true
    },
    f: {
      alias: "filter",
      describe: "Filter logs by evaluating javascript expression. `log` can be accessed to get properties of the " +
          "current record, e.g. `log.level`; see https://gitlab.com/oddlog/record#record-exports for details. The " +
          "level values `TRACE`, `INFO`, etc. can be accessed as well. The expression is expected to return a truthy " +
          "value for the log to be written, a falsy value otherwise. Simple expressions (contains no `;`) do not " +
          "need the `return` keyword. Multiple expressions are connected with an AND operator, thus the log is only " +
          "written if all the expressions return truthy.",
      array: true
    },
    r: {
      alias: "reject",
      describe: "Similar to the `--filter`, but reject logs on truthy return and vice versa. Multiple expressions " +
          "are connected with an OR operator, thus the log is only written if none of the expressions returns truthy.",
      array: true
    },
    i: {
      alias: "ignore-invalid",
      describe: "Do not write lines that were not recognized by oddlog.",
      boolean: false
    },
  },

  groups: [
    {commands: ["level", "date", "name"], describe: "Message filter options:"},
    {commands: ["invalid", "line-separator"], describe: "Misc options:"},
  ],

  examples: [
    {
      command: "oddlog-filter -a 'my-app:**' '!my-app:void' < log-file",
      describe: "Filter logs with logger name prefix `my-app:` but not `my-app:void`."
    },
    {
      command: "oddlog-filter -d '2016-12-14T16:20:45.895Z' < log-file",
      describe: "Filter logs with exact iso date match."
    },
    {
      command: "oddlog-filter -d '12-14 16:20' < log-file",
      describe: "Filter logs within the minute of 16:20 on the 14th of december of any year."
    },
    {command: "oddlog-filter -d '03:21:45.895' < log-file", describe: "Filter logs with exact time match."},
  ],

};

/*------------------------------------------------------ Format ------------------------------------------------------*/

export const format = {

  commands: {
    f: {
      alias: "format",
      describe: "The output format",
      choices: ["json", "csv"],
      default: "json"
    },
    d: {
      alias: "date",
      describe: "The date format",
      choices: ["time", "iso", "timestamp", "unix", "relative"],
      default: "iso"
    },
    FS: {
      alias: "field-separator",
      describe: "The field separator (csv format)",
      default: ","
    },
  },

  groups: [],

  examples: [
    {command: "oddlog-format < log-file", describe: "Format logs as json objects."},
    {command: "oddlog-format -f csv -d timestamp --FS='\t' < log-file", describe: "Format logs as tab-separated CSV."},
  ],

};

/*------------------------------------------------------ Pretty ------------------------------------------------------*/

const verbosityGroup = [];

export const pretty = {

  commands: {
    c: {
      alias: "colors",
      describe: "The color scheme to use.",
      choices: ["default", "discreet", "off"],
      default: "default"
    },
    d: Object.assign({}, format.commands.d, {default: "time"}),
    // n: {
    //   alias: ["newline"],
    //   describe: "Separate records with an empty line.",
    //   boolean: true
    // },
    v: {
      describe: "Increase verbosity (default: 3, min: 0, max: 5) for all levels that ain't overwritten by level " +
          "specific verbosity options.",
      count: true
    },
    r: {
      describe: "Reduce verbosity (default: 3, min: 0, max: 5) for all levels that ain't overwritten by level " +
          "specific verbosity options.",
      count: true
    }
  },

  groups: [
    {commands: ["style", "date", "newline"], describe: "Output format modifiers:"},
    {commands: ["verbose"].concat(verbosityGroup), describe: "Pretty format options:"},
  ],

  examples: [
    {command: "tail -f logs/my-logs.log | $0", describe: "Output pretty logs as they are written."},
    {command: "tail -f logs/my-logs.log | $0 -vvvvv", describe: "Use highest verbosity."},
  ],

};

for (let i = 0; i < LEVELS.length; i++) {
  const levelName = LEVELS[i];
  pretty.commands[levelName[0].toUpperCase()] = {
    alias: "verbosity-" + levelName,
    describe: "Set the verbosity of the level " + levelName.toUpperCase() + " and above.",
    nargs: 1,
    number: true
  };
  verbosityGroup.push("verbosity-" + levelName);
}

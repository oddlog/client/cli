import {LIST as LEVELS} from "@oddlog/levels";

import {padLeft} from "../services/pad";

const PADDED = {};

/*===================================================== Exports  =====================================================*/

export {padded};

/*================================================ Initial Execution  ================================================*/

let MAX_LENGTH = 0;
for (let name of LEVELS) { if ((name.length) > MAX_LENGTH) { MAX_LENGTH = name.length; } }

for (let i = 0; i < LEVELS.length; i++) { PADDED[i] = padLeft(LEVELS[i].toUpperCase(), " ", MAX_LENGTH); }

/*==================================================== Functions  ====================================================*/

function padded(value) { return PADDED.hasOwnProperty(value) ? PADDED[value] : padLeft(value, " ", MAX_LENGTH); }

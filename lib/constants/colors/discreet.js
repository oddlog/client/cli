import chalk from "chalk";

/*===================================================== Exports  =====================================================*/

export const LEVEL = {
  silent: (text) => chalk.dim(text),
  trace: (text) => chalk.gray(text),
  debug: (text) => chalk.green(text),
  verbose: (text) => chalk.cyan(text),
  info: (text) => chalk.blue(text),
  warn: (text) => chalk.yellow(text),
  error: (text) => chalk.red(text),
  fatal: (text) => chalk.bgRed(text)
};

export const LINE = {_fallback: (text) => "  " + text}; // add one indentation level to block lines

export const MESSAGE = {_fallback: (text) => chalk.bold(text)};
export const NO_MESSAGE = {_fallback: (text) => chalk.bold(text)};
export const ERROR_STACK = {_fallback: (text) => chalk.red(text)};

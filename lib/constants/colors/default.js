import chalk from "chalk";

const VALUE = {_fallback: (text) => chalk.yellow(text)};

/*===================================================== Exports  =====================================================*/

export const LEVEL = {
  silent: (text) => chalk.dim(text),
  trace: (text) => chalk.gray(text),
  debug: (text) => chalk.green(text),
  verbose: (text) => chalk.cyan(text),
  info: (text) => chalk.blue(text),
  warn: (text) => chalk.yellowBright(text),
  error: (text) => chalk.red.bold(text),
  fatal: (text) => chalk.bgRed.bold(text)
};

export const LINE = {
  trace: (text) => chalk.bgWhite(" ") + " " + text,
  debug: (text) => chalk.bgGreen(" ") + " " + text,
  verbose: (text) => chalk.bgCyan(" ") + " " + text,
  info: (text) => chalk.bgBlue(" ") + " " + text,
  warn: (text) => chalk.bgYellowBright(" ") + " " + text,
  error: (text) => chalk.bgRed(" ") + " " + text,
  fatal: (text) => chalk.bgRed(" ") + " " + text,
  _fallback: (text) => "  " + text
};

export const LINE_HEAD = {_fallback: (text) => text};
export const SOURCE = {_fallback: (text) => text};
export const TIME = {_fallback: (text) => chalk.inverse(text)};
export const META = {_fallback: (text) => chalk.dim(text)};
export const NAME = {_fallback: (text) => chalk.italic(text)};
export const MESSAGE = {_fallback: (text) => chalk.bold(text)};
export const NO_MESSAGE = {_fallback: (text) => chalk.bold(text)};
export const NATIVE_INLINE = VALUE;
export const NATIVE_MULTI_LINE = VALUE;
export const ERROR_STACK = {_fallback: (text) => chalk.red(text)};
export const OBJECT_KEY = {_fallback: (text) => chalk.magenta(text)};
export const OBJECT_COLON = {};
export const OBJECT_FS = {};
export const OBJECT_BRACE = {_fallback: (text) => chalk.green(text)};

export const TYPES = {
  NULL: VALUE,
  boolean: VALUE,
  string: VALUE,
  number: VALUE
};

export const ARRAY_KEY = OBJECT_KEY;
export const ARRAY_INLINE_KEY = OBJECT_KEY;
export const OBJECT_INLINE_KEY = OBJECT_KEY;

export const ARRAY_COLON = OBJECT_COLON;
export const ARRAY_INLINE_COLON = OBJECT_COLON;
export const OBJECT_INLINE_COLON = OBJECT_COLON;

export const ARRAY_FS = OBJECT_FS;
export const ARRAY_INLINE_FS = OBJECT_FS;
export const OBJECT_INLINE_FS = OBJECT_FS;

export const ARRAY_BRACE = OBJECT_BRACE;
export const ARRAY_INLINE_BRACE = OBJECT_BRACE;
export const OBJECT_INLINE_BRACE = OBJECT_BRACE;

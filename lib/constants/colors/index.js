import * as colorsDefault from "./default";
import * as colorsDiscreet from "./discreet";
import * as colorsOff from "./off";

/*===================================================== Exports  =====================================================*/

export {
  colorsDefault as default,
  colorsDiscreet as discreet,
  colorsOff as off,
};

import os from "os";
import yargs from "yargs";

/*===================================================== Exports  =====================================================*/

export default parse;

/*==================================================== Functions  ====================================================*/

function parse(argv, cmd, definition) {
  try {
    const args = yargs(argv)
        .usage("Usage: oddlog " + cmd + " [options]" + os.EOL + "Reads from stdin and writes formatted logs to stdout.")
        .env("ODDLOG")
        .options(definition.commands)
        .help("h")
        .alias("h", "help")
        .config()
        .version(false)
        .describe("h", "Show this help message.");
    for (let group of definition.groups) { args.group(group.commands, group.describe); }
    for (let example of definition.examples) { args.example(example.command, example.describe); }
    return args.argv;
  } catch (err) {
    process.stderr.write(err.stack);
    process.exit(1);
  }
}

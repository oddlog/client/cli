import {padLeft} from "./pad";

const DATE_FORMAT = /(\d{1,4}-)?\d{1,2}-\d{1,2}/;
const TZD_FORMAT = /(Z|[+-]\d{1,2}(:\d{1,2})?)/;
const TIME_FORMAT = /(\d{1,2}(:\d{1,2}(:\d{1,2}(\.\d{1,3})?)?)?)?/;
const DATE_TIME_FORMAT = new RegExp(
    "^" + DATE_FORMAT.source + "((T|\\s+)" + TZD_FORMAT.source + ")?|(" +
    TIME_FORMAT.source + "|" +
    "(" + DATE_FORMAT.source + "T|\\s+" + TIME_FORMAT.source + ")" +
    ")" + TZD_FORMAT.source + "?$"
);
const VOID_MATCHER = {matches: () => true};

/*===================================================== Exports  =====================================================*/

export {getDateFilter, formatDate};

/*==================================================== Functions  ====================================================*/

function formatDate(date, prevDate, format) {
  switch (format) {
    case "time":
      return (padLeft(date.getHours(), 0, 2)) +
          ":" + (padLeft(date.getMinutes(), 0, 2)) +
          ":" + (padLeft(date.getSeconds(), 0, 2)) +
          "." + (padLeft(date.getMilliseconds(), 0, 3));
    case "iso":
      return date.toISOString();
    case "timestamp":
      return date.getTime();
    case "unix":
      return (date.getTime() / 1000) | 0;
    case "relative":
      if (prevDate == null) { return ""; }
      return prevDate >= date ? "+" + (prevDate - date) + "ms" : "-" + (date - prevDate) + "ms";
    default:
      throw new Error("Date format '" + format + "' not supported.");
  }
}

function getDateFilter(source) {
  /*
   * DATE_TIME_FORMAT := DATE_FORMAT([T ]TZD)? | ( TIME_FORMAT | DATE_FORMAT[T ]TIME_FORMAT )(TZD)?
   * DATE_FORMAT  := ((YEAR-)?XX-)?XX
   * TIME_FORMAT  := XX(:XX(:XX(.XXX)?)?)?
   * Y    := [0123456789]
   * XX   := Y?Y
   * XXX  := Y?Y?Y
   * YEAR := ((Y?Y)?Y?)Y
   * TZD  := [+-]H?H(:m?m)?
   */
  if (source == null || (source = source.trim()).length === 0) { return VOID_MATCHER; }
  if (!DATE_TIME_FORMAT.test(source)) { throw new Error("Date parameter '" + source + "' invalid."); }
  // allow whitespace for T separator
  source = source.replace(/\s+/, "T");
  // fill single-digit numbers with leading zeroes (except ms)
  source = source.replace(/(^|[^\d.])(\d)(?=\D|$)/g, "$10$2");
  // process date string
  let {date, time, tzd} = splitDateTime(source);
  let offset = getTimezoneOffset(tzd);
  let matcher = new RegExp(date + "T" + time.replace(/\./, "\\."));
  return {offset, matcher, matches: createMatcherFunction(offset, matcher)};
}

function getTimezoneOffset(tzdString) {
  if (tzdString == null) { return new Date().getTimezoneOffset(); } else if (tzdString === "Z") { return 0; }
  let split = tzdString.substring(1).split(":");
  return (tzdString[0] === "+" ? -1 : 1) * ((+split[0] * 60) + (split[1] || 0));
}

function createMatcherFunction(offset, matcher) {
  return (message) => {
    let date;
    if (offset) {
      date = new Date(message.date.getTime());
      date.setMinutes(date.getMinutes() - offset);
    } else {
      date = message.date;
    }
    return matcher.test(date.toISOString());
  };
}

function splitDateTime(dateTimeString) {
  let date, time, tzd, split = dateTimeString.split("T", 2);
  if (split.length < 2) {
    tzd = null; // no timezone allowed without T separator
    if (dateTimeString.includes("-")) {
      [date, time] = [dateTimeString, ""];
    } else {
      [date, time] = ["", dateTimeString];
    }
  } else {
    [date, time] = [split[0], split[1] || ""];
    split = time.replace(/([Z+-])/, "T$1").split("T", 2);
    [time, tzd] = [split[0], split[1] || null];
  }
  return {date, time, tzd};
}

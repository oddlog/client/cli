import {formatDate} from "../date";

/*===================================================== Exports  =====================================================*/

export default format;

/*==================================================== Functions  ====================================================*/

// eslint-disable-next-line complexity
function format(log, prevLog, options) {
  // [RFC 4180](https://tools.ietf.org/html/rfc4180)
  const date = formatDate(log.date, prevLog && prevLog.date, options.date);
  return [
    csvWrapString(log.schemaVersion),
    csvWrapString(log.typeKey || ""),
    csvStringify(log.metaInput),
    csvWrapString(log.name),
    typeof date === "string" ? csvWrapString(date) : date,
    log.level,
    log.source && csvWrapString(log.source.file) || "",
    log.source && log.source.row || "",
    log.source && log.source.col || "",
    log.message && csvWrapString(log.message) || "",
    log.payload === void 0 ? "" : csvStringify(log.payload),
  ].join(options.FS);
}

function csvStringify(obj) { return csvWrapString(JSON.stringify(obj)); }

function csvWrapString(str) { return "\"" + escape(str.toString()) + "\""; }

/**
 * Creates an escaped version of the passed string. Char codes below 32 (non-printed chars) are stripped.
 * This function differs from @oddlog/utils since it does not escape backslashes and escapes double-quotes as `""`.
 *
 * @param {String} str The input string.
 * @returns {String} The escaped string.
 */
function escape(str) {
  let result = "";
  let someEscape = false;
  let lastIdx = 0;
  const _len = str.length;
  for (let i = 0; i < _len; i++) {
    const code = str.charCodeAt(i);
    if (code === 0x22 /* DOUBLE_QUOTE */) {
      result += str.slice(lastIdx, i) + "\"";
      lastIdx = i;
      someEscape = true;
    } else if (code < 32) {
      result += str.slice(lastIdx, i);
      lastIdx = i + 1;
    }
  }
  return someEscape ? result + str.slice(lastIdx) : str;
}

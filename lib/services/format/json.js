import {formatDate} from "../date";

/*===================================================== Exports  =====================================================*/

export default format;

/*==================================================== Functions  ====================================================*/

function format(log, prevLog, options) {
  return JSON.stringify({
    schemaVersion: log.schemaVersion,
    typeKey: log.typeKey,
    meta: log.meta,
    name: log.name,
    date: formatDate(log.date, prevLog && prevLog.date, options.date),
    level: log.level,
    source: log.source,
    message: log.message,
    payload: log.payload,
  });
}

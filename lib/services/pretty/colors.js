export {style, styleNative};

/*==================================================== Functions  ====================================================*/

function style(text, log, colors) {
  if (colors == null) { return text; }
  if (colors.hasOwnProperty(log.levelName)) { return colors[log.levelName](text, log); }
  if (colors.hasOwnProperty("_fallback")) { return colors._fallback(text, log); }
  return text;
}

function styleNative(text, log, colors) {
  if (typeof text === "object" && text !== null) { throw new Error("Objects ain't native value"); }
  if (colors.TYPES == null) { return text; }
  return style(text, log, colors.TYPES[text === null ? "NULL" : typeof text]);
}

import os from "os";
import {escape} from "@oddlog/utils";

import {padLeft} from "../pad";
import {padded as paddedLevel} from "../../constants/levels";
import {formatNative, getPayloadString, splitPayload} from "./payload";
import {style, styleNative} from "./colors";
import {formatDate} from "../date";

/*
 * Levels of verbosity:
 *
 * 0: [date] VERBOSE:  name  message
 * 1: [date] VERBOSE:  name  message
 *      optional file location
 *      optional error stack
 * 2: [date] VERBOSE:  name  message  inline-payload
 *      optional file location
 *      optional error stack
 * 3: [date] VERBOSE:  name  message  inline-payload
 *      multi-line payload
 *      optional file location
 *      optional error stack
 * 4: [date] VERBOSE/03:  name  message
 *      payload
 *      optional file location
 *      optional error stack
 * 5: [date] VERBOSE/03:  [optional meta data]  name  message
 *      payload
 *      optional file location
 *      optional error stack
 */

const MIN_VERBOSITY_ERROR_STACK = 1, MIN_VERBOSITY_LOCATION = 1;
const MIN_VERBOSITY_PAYLOAD_INLINE = 2, MIN_VERBOSITY_PAYLOAD_MULTI_LINE = 3, MIN_VERBOSITY_PAYLOAD_EXTRA = 4;
const MIN_VERBOSITY_LEVEL_NUM = 4;
const MIN_VERBOSITY_META = 5;

/*===================================================== Exports  =====================================================*/

export default format;

/*==================================================== Functions  ====================================================*/

function format(log, prevLog, options) {
  const verbosity = options.verbosity[log.floorLevelName];
  const {payloadErrors, payloadInline, payloadMultiLine} = splitPayload(log, verbosity >= MIN_VERBOSITY_PAYLOAD_EXTRA);

  let result = style(getHeadline(log, prevLog, verbosity, options, payloadInline), log, options.colors.LINE_HEAD);
  if (verbosity >= MIN_VERBOSITY_PAYLOAD_MULTI_LINE) {
    const str = getPayloadString(log, options, payloadMultiLine, 80);
    if (str !== void 0) { result += str; }
  }
  if (verbosity >= MIN_VERBOSITY_LOCATION) {
    const str = getLocation(log, options);
    if (str !== void 0) { result += os.EOL + str; }
  }
  if (verbosity >= MIN_VERBOSITY_ERROR_STACK) {
    for (let i = 0; i < payloadErrors.length; i++) {
      const str = getErrorStack(log, options, payloadErrors[i]);
      if (str !== void 0) { result += os.EOL + str; }
    }
  }
  return result;
}

function getHeadline(log, prevLog, verbosity, options, payload) {
  let result = "";
  result += getDate(log, prevLog, options) + " ";
  result += getLevel(log, verbosity, options) + ":  ";
  if (verbosity >= MIN_VERBOSITY_META) {
    const str = getMeta(log, options);
    if (str !== void 0) { result += str + "  "; }
  }
  result += getName(log, options) + "  ";
  result += getMessage(log, options);
  if (verbosity >= MIN_VERBOSITY_PAYLOAD_INLINE && verbosity < MIN_VERBOSITY_PAYLOAD_EXTRA) {
    const str = getPayloadInline(log, options, payload);
    if (str !== void 0) { result += "  " + str; }
  }
  return result;
}

/*------------------------------------------------ simple properties  ------------------------------------------------*/

function getDate(log, prevLog, options) {
  return style("[" + formatDate(log.date, prevLog && prevLog.date, options.date) + "]", log, options.colors.TIME);
}

function getLevel(log, verbosity, options) {
  let result = paddedLevel(log.level);
  if (verbosity >= MIN_VERBOSITY_LEVEL_NUM) { result += "/" + padLeft(log.level, 0, 2); }
  return style(result, log, options.colors.LEVEL);
}

function getMeta(log, options) {
  // todo check log.isCustomMeta
  const meta = log.meta;
  if (meta != null) {
    let result = "[";
    if (meta.host) { result += meta.host; }
    if (meta.platform || meta.pid) { result += "/"; }
    if (meta.pid) { result += meta.pid + (meta.platform ? " " : ""); }
    if (meta.platform) { result += meta.platform; }
    result += "]";
    return style(result, log, options.colors.META);
  }
}

function getName(log, options) { return style(log.name, log, options.colors.NAME); }

function getMessage(log, options) {
  if (log.message == null) { return style(" ", log, options.colors.NO_MESSAGE); }
  return style(log.message, log, options.colors.MESSAGE);
}

function getLocation(log, options) {
  if (log.sourceInput != null) {
    const sourceStr = style(log.sourceInput.join(":"), log, options.colors.SOURCE);
    return style(sourceStr, log, options.colors.LINE);
  }
}

/*----------------------------------------------------- payload  -----------------------------------------------------*/

function getErrorStack(log, options, payload) {
  if (payload == null || typeof payload.stack !== "string") { return; }
  let result = "";
  const lines = payload.stack.split(/\r?\n\r?/);
  for (let i = 0; i < lines.length; i++) {
    const line = style(lines[i], log, options.colors.ERROR_STACK);
    if (i) { result += os.EOL; }
    result += style(line, log, options.colors.LINE);
  }
  return result;
}

function getPayloadInline(log, options, payload) {
  if (payload === void 0) { return; }
  if (typeof payload !== "object" || payload === null) {
    return style(formatNative(payload), log, options.colors.NATIVE_INLINE);
  }
  let result = "";
  if (Array.isArray(payload)) {
    result += style("[", log, options.colors.ARRAY_INLINE_BRACE);
    for (let i = 0; i < payload.length; i++) {
      if (i) { result += style(", ", log, options.colors.ARRAY_INLINE_FS); }
      result += style(i, log, options.colors.ARRAY_INLINE_KEY);
      result += style(":", log, options.colors.ARRAY_INLINE_COLON);
      result += styleNative(formatNative(payload[i]), log, options.colors);
    }
    result += style("]", log, options.colors.ARRAY_INLINE_BRACE);
  } else {
    let first = true;
    result += style("{", log, options.colors.OBJECT_INLINE_BRACE);
    for (let key in payload) {
      if (!first) { result += style(", ", log, options.colors.OBJECT_INLINE_FS); }
      first = false;
      result += style(escape(key), log, options.colors.OBJECT_INLINE_KEY);
      result += style(":", log, options.colors.OBJECT_INLINE_COLON);
      result += styleNative(formatNative(payload[key]), log, options.colors);
    }
    result += style("}", log, options.colors.OBJECT_INLINE_BRACE);
  }
  return result;
}

/* eslint complexity: "off" */
import os from "os";
import {escape} from "@oddlog/utils";

import {style} from "./colors";
import {padLeft} from "../pad";

const MAX_INLINE_VALUE_LENGTH = 30;

/*===================================================== Exports  =====================================================*/

export {splitPayload, formatNative, getPayloadString};

/*==================================================== Functions  ====================================================*/

function formatNative(value) { return JSON.stringify(value); }

function splitPayload(message, noInline) {
  const typeKey = message.typeKey;
  let payload = message.payload;
  let payloadErrors = [], payloadInline = void 0, payloadMultiLine = void 0;
  payload = resolveType(payloadErrors, typeKey, payload, true, false);
  if (payload === null) {
    if (noInline) { payloadMultiLine = null; } else { payloadInline = null; }
  } else if (payload != null) {
    if (typeof payload === "object") {
      if (Array.isArray(payload)) {
        assignPayloadArray(payload);
      } else {
        for (let key in payload) { //noinspection JSUnfilteredForInLoop
          assignPayloadValue(key, payload[key]);
        }
      }
    } else {
      if (noInline) { payloadMultiLine = payload; } else { payloadInline = payload; }
    }
  }
  return {payloadErrors, payloadInline, payloadMultiLine};

  function assignPayloadArray(arr) {
    let allNative = true;
    for (let i = 0; i < arr.length; i++) {
      let value = arr[i];
      if (value != null && (typeof value === "object" || value.length > 30)) {
        allNative = false;
        break;
      }
    }
    if (allNative && !noInline) { payloadInline = arr; } else { payloadMultiLine = arr; }
  }

  function assignPayloadValue(key, value) {
    if (!noInline && value != null && typeof value !== "object" && value.toString().length > MAX_INLINE_VALUE_LENGTH) {
      // too long value, show short inline and complete multi-line
      if (payloadMultiLine == null) { payloadMultiLine = {[key]: value}; } else { payloadMultiLine[key] = value; }
      value = value.toString().substr(0, MAX_INLINE_VALUE_LENGTH - 3) + "...";
      if (payloadInline == null) { payloadInline = {[key]: value}; } else { payloadInline[key] = value; }
    } else if (noInline || typeof value === "object" && value !== null) {
      if (payloadMultiLine == null) { payloadMultiLine = {[key]: value}; } else { payloadMultiLine[key] = value; }
    } else {
      if (payloadInline == null) { payloadInline = {[key]: value}; } else { payloadInline[key] = value; }
    }
  }
}

function resolveType(payloadErrors, typeKey, obj, root, isErrorKey) {
  if (typeof obj !== "object" || obj === null) { return obj; }
  if (Array.isArray(obj)) {
    return resolveTypeArray(payloadErrors, typeKey, obj);
  } else {
    let hasType = obj.hasOwnProperty(typeKey) && typeof obj[typeKey] === "string";
    let type = hasType ? obj[typeKey].toLowerCase() : null;
    if (isErrorKey && !hasType || type === "error") {
      // strip error properties out of object
      let pError = {stack: obj.stack};
      payloadErrors.push(pError);
      Reflect.deleteProperty(obj, "stack");
      // "remove" obj from parent iff no further properties
      if (!Object.keys(obj).length) { return void 0; }
    } else if (type === "value") {
      return resolveType(payloadErrors, typeKey, obj._value, root, isErrorKey);
    } else if (type === "plain") {
      Reflect.deleteProperty(obj, typeKey);
    }
    for (let key in obj) {
      //noinspection JSUnfilteredForInLoop
      let value = obj[key];
      //noinspection JSUnfilteredForInLoop
      value = obj[key] = resolveType(payloadErrors, typeKey, value, false, root && (key === "err" || key === "error"));
      if (value === void 0) {
        //noinspection JSUnfilteredForInLoop
        Reflect.deleteProperty(obj, key);
      }
    }
    return obj;
  }
}

function resolveTypeArray(payloadErrors, typeKey, array) {
  for (let i = 0; i < array.length; i++) { array[i] = resolveType(payloadErrors, typeKey, array[i], false, false); }
  return array;
}

function getPayloadString(message, options, payload, maxLength) {
  if (payload === void 0) { return; }
  if (typeof payload !== "object" || payload === null) {
    return os.EOL + style(formatNative(payload), message, options.colors.NATIVE_MULTI_LINE);
  }
  let result = getPayloadRecursive(message, options, payload, maxLength, "");
  if (result != null) {
    if (result.length === null) { return result.string; }
    return os.EOL + style(result.string, message, options.colors.LINE);
  }
}

function getPayloadRecursive(message, options, payload, maxLength, indent) {
  if (Array.isArray(payload)) {
    return getArrayPayloadRecursive(message, options, payload, maxLength, indent);
  }
  return getObjectPayloadRecursive(message, options, payload, maxLength, indent);
}

function getArrayPayloadRecursive(message, options, payload, maxLength, indent) {
  let inlineLength = 1;
  let inlineString = style("[", message, options.colors.ARRAY_BRACE);
  let multiLineString = "";
  let value, multiLineStr;
  let keyLength = (payload.length - 1).toString().length;
  for (let i = 0; i < payload.length; i++) {
    if (i) { inlineString += style(", ", message, options.colors.ARRAY_FS); }
    multiLineString += os.EOL;
    multiLineStr = indent + style(padLeft(i, " ", keyLength), message, options.colors.ARRAY_KEY) + ":";
    value = payload[i];
    if (typeof value === "object" && payload !== null) {
      value = getPayloadRecursive(message, options, value, maxLength - keyLength - 3, indent + "  ", false);
      if (value.length === null || inlineLength !== null && (inlineLength += value.length) > maxLength) {
        inlineLength = null;
      }
      if (inlineLength === null) {
        multiLineString += style(multiLineStr, message, options.colors.LINE) + value.string;
      } else {
        inlineString += value.string;
        multiLineString += style(multiLineStr + value.string, message, options.colors.LINE);
      }
    } else {
      value = formatNative(value);
      if (inlineLength !== null && (inlineLength += value.length) > maxLength) {
        inlineLength = null;
      }
      value = style(value, message, options.colors.NATIVE_MULTI_LINE);
      inlineString += value;
      multiLineStr += " " + value;
      multiLineString += style(multiLineStr, message, options.colors.LINE);
    }
  }
  if (inlineLength !== null && ++inlineLength <= maxLength) {
    inlineString += style("]", message, options.colors.ARRAY_BRACE);
    return {length: inlineLength, string: inlineString};
  }
  return {length: null, string: multiLineString};
}

function getObjectPayloadRecursive(message, options, payload, maxLength, indent) {
  let inlineLength = 1;
  let inlineString = style("{", message, options.colors.OBJECT_BRACE);
  let multiLineString = "";
  let value, multiLineStr, keyString, keyStringFormatted;
  let first = true;
  for (let key in payload) {
    if (payload.hasOwnProperty(key)) {
      if (!first) { inlineString += style(", ", message, options.colors.OBJECT_FS); }
      keyString = escape(key);
      if (inlineLength !== null) { inlineLength += keyString.length + 2; }
      keyStringFormatted = style(keyString, message, options.colors.OBJECT_KEY);
      keyStringFormatted += style(": ", message, options.colors.OBJECT_COLON);
      inlineString += keyStringFormatted;
      first = false;
      multiLineString += os.EOL;
      multiLineStr = indent + keyStringFormatted;
      value = payload[key];
      if (typeof value === "object" && payload !== null) {
        value = getPayloadRecursive(message, options, value, maxLength - keyString.length - 2, indent + "  ");
        if (value.length === null || inlineLength !== null && (inlineLength += value.length) > maxLength) {
          inlineLength = null;
        }
        if (inlineLength === null) {
          multiLineString += style(multiLineStr, message, options.colors.LINE) + value.string;
        } else {
          inlineString += value.string;
          multiLineString += style(multiLineStr + value.string, message, options.colors.LINE);
        }
      } else {
        value = formatNative(value);
        if (inlineLength !== null && (inlineLength += value.length) > maxLength) {
          inlineLength = null;
        }
        value = style(value, message, options.colors.NATIVE_MULTI_LINE);
        inlineString += value;
        multiLineStr += value;
        multiLineString += style(multiLineStr, message, options.colors.LINE);
      }
    }
  }
  if (inlineLength !== null && ++inlineLength <= maxLength) {
    inlineString += style("}", message, options.colors.OBJECT_BRACE);
    return {length: inlineLength, string: inlineString};
  }
  return {length: null, string: multiLineString};
}

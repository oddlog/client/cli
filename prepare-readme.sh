#!/usr/bin/env bash

ln -s ./build/cjs/bin/index.js ./oddlog # use "oddlog" for $0 within help message
help="$(./oddlog --help | sed 's/\[default: "$/\[default: EOL\]/' | grep -v '^[[:space:]]*"\]$')"
rm ./oddlog

prog='BEGIN {slice=0} /^<!-- \[\/HELP GENERATED\] -->/{slice=0} /^<!-- \[HELP GENERATED\] -->$/{slice=1; print $0; print help;} {if (!slice) { print $0 } }'

help="\`\`\`\n$help\n\`\`\`"

gawk -v help="$help" -i inplace "$prog" README.md

# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/) 
and this project adheres to [Semantic Versioning](http://semver.org/).

## [Unreleased]

## [1.0.0] - 2018-11-07

### Changed

- Reworked usage

## [0.7.1] - 2018-08-01

### Added
- Limit inline payload length to 30 characters. If this limit is exceeded, show excerpt inline and complete value
  multi-line

## [0.7.0] - 2018-08-01

### Added
- Support log record schema v3

### Changed
- CSV output now uses only one JSON meta field

### Fixed
- CSV output source location throwing an error

## [0.6.1] - 2018-07-25

### Added
- Support log record schema v2 (no custom meta support yet)

## [0.6.0] - 2017-11-13

### Changed
- Use [ns-matcher](https://www.npmjs.com/package/ns-matcher) in favor of
  [micromatch](https://www.npmjs.com/package/micromatch) due to in-order interpretation of namespaces.
- Updated dependencies to latest

## [0.5.1] - 2016-12-14

### Changed
- CSV output surround date with quotes to allow more separators to work

### Fixed
- CSV output quotes of logger name

## [0.5.0] - 2016-12-14

### Added
- `--application` parameter to filter messages by logger name
- oddlog record schema v1 implementation (see
  https://gitlab.com/frissdiegurke/oddlog/wikis/developer-message-record#version-1)

## [0.4.1] - 2016-12-14

### Fixed
- *README.md* not included in release

## [0.4.0] - 2016-12-14

### Added
- `--config` parameter to use a JSON file for configuration
- `--version` parameter to show package version
- `--date` parameter to filter messages by date
- support of environment variables for configuration

### Changed
- `--date` and `-d` are no longer aliases of `--iso-date` but of new date filter

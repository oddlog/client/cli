# oddlog command-line interface

[![License](https://img.shields.io/npm/l/oddlog-cli.svg)](LICENSE)
[![Version](https://img.shields.io/npm/v/oddlog-cli.svg)](https://www.npmjs.com/package/oddlog-cli)

## Preview

[![Preview 01](https://gitlab.com/oddlog/assets/raw/master/readme-01.png)](https://gitlab.com/oddlog/assets/raw/master/readme-01.png).png)

## Usage

<!-- [HELP GENERATED] -->
```
Usage: oddlog [CMD] [options...]

CMD may be one of
  filter - filter logs.
  pretty - pretty-format logs.
  format - format logs for further processing.
If none is specified, 'pretty' is used.

Check `oddlog CMD --help` for further help.
```
<!-- [/HELP GENERATED] -->

## License

The source code is licensed under [MIT](https://gitlab.com/oddlog/client/cli/blob/master/LICENSE). If you don't
agree to the license, you may not contribute to the project. Feel free to fork and maintain your custom build thought.

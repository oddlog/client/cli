#!/usr/bin/env node

import {SCHEMA_VERSION, trapSignals} from "@oddlog/cli-utils";

import {name, version} from "../../../package.json"; // build/cjs/bin/index.js
import filter from "./filter";
import pretty from "./pretty";
import format from "./format";

/*================================================ Initial Execution  ================================================*/

trapSignals();

switch (process.argv[2]) { /* eslint-disable no-console */
  case "-h":
  case "--help":
    console.log("Usage: oddlog [CMD] [options...]");
    console.log("");
    console.log("CMD may be one of");
    console.log("  filter - filter logs.");
    console.log("  pretty - pretty-format logs.");
    console.log("  format - format logs for further processing.");
    console.log("If none is specified, 'pretty' is used.");
    console.log("");
    console.log("Check `oddlog CMD --help` for further help.");
    break;
  case "--version":
    console.log(name, version);
    console.log("Supported record schema version:", SCHEMA_VERSION);
    break;
  case "filter":
    process.argv.splice(2, 1);
    filter();
    break;
  case "pretty":
    process.argv.splice(2, 1);
    pretty();
    break;
  case "format":
    process.argv.splice(2, 1);
    format();
    break;
  default:
    pretty();
    break;
}

import {pipe} from "@oddlog/cli-utils";

import {format as optionDef} from "../lib/constants/argument-options";
import parseArgv from "../lib/services/parse-argv";
import csv from "../lib/services/format/csv";
import json from "../lib/services/format/json";

/*===================================================== Exports  =====================================================*/

export default exec;

/*==================================================== Functions  ====================================================*/

function exec(argv) {
  const options = parseArgv(argv || process.argv, "format", optionDef);
  const formatter = options.format === "csv" ? csv : json;

  let prevLog = null;
  pipe({
    onLog: (log, printLn) => {
      printLn(formatter(log, prevLog, options));
      prevLog = log;
    }
  });
}

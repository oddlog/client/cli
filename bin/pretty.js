import {pipe} from "@oddlog/cli-utils";
import {LIST as LEVELS} from "@oddlog/levels";

import {pretty as optionDef} from "../lib/constants/argument-options";
import parseArgv from "../lib/services/parse-argv";
import pretty from "../lib/services/pretty";
import * as colors from "../lib/constants/colors";

/*===================================================== Exports  =====================================================*/

export default exec;

/*==================================================== Functions  ====================================================*/

function exec(argv) {
  const options = parseOptions(argv || process.argv);

  let prevLog = null;
  pipe({
    onLog: (log, printLn) => {
      printLn(pretty(log, prevLog, options));
      prevLog = log;
    }
  });
}

function parseOptions(argv) {
  const options = parseArgv(argv || process.argv, "[pretty]", optionDef);
  options.colors = colors[options.colors];
  options.verbosity = getVerbosity(options);
  return options;
}

function getVerbosity(args) {
  const result = {};

  let verbosity = 3 + (args.v || 0) - (args.r || 0);
  for (let i = 0; i < LEVELS.length; i++) {
    const levelName = LEVELS[i];
    const v = args["verbosity-" + levelName];
    if (typeof v === "number" && !Number.isNaN(v) && v >= 0) { verbosity = Math.ceil(v); }
    result[levelName] = verbosity;
  }

  return result;
}

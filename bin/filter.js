import nsMatcher from "ns-matcher";
import {pipe} from "@oddlog/cli-utils";
import {getLevelValue, LIST as LEVELS, MAPPED as LEVEL_MAP} from "@oddlog/levels";

import {filter as optionDef} from "../lib/constants/argument-options";
import {getDateFilter} from "../lib/services/date";
import parseArgv from "../lib/services/parse-argv";

/*===================================================== Exports  =====================================================*/

export default exec;

/*==================================================== Functions  ====================================================*/

function exec(argv) {
  const options = parseOptions(argv || process.argv);

  pipe({
    onLog: (log, write) => filter(log, options) && write(log),
    invalid: options.invalid,
  });
}

function parseOptions(argv) {
  try {
    const options = parseArgv(argv || process.argv, "filter", optionDef);
    return {
      invalid: !options["ignore-invalid"],
      level: getLevelValue(options.level),
      date: getDateFilter(options.date),
      name: getNameFilter(options.name),
      filter: parseFiltersAnd(options.filter || []),
      reject: parseFiltersOr(options.reject || []),
    };
  } catch (err) {
    process.stderr.write(err.stack);
    process.exit(1);
  }
}

function filter(log, options) {
  if (options.level != null && log.level < options.level) { return false; }
  if (options.date != null && !options.date.matches(log)) { return false; }
  if (options.name != null && !options.name.test(log.name)) { return false; }
  if (!options.filter(log) || options.reject(log)) { return false; }
  //
  return true;
}

function getNameFilter(array) {
  let patterns = array == null || !array.length ? process.env.DEBUG || null : array.join(",");
  return patterns && nsMatcher(patterns);
}

function parseFiltersAnd(filterStrings) {
  const functions = filterStrings.map(parseFilter);
  return (record) => {
    for (let fn of functions) { if (!fn(record)) { return false; } }
    return true;
  };
}

function parseFiltersOr(filterStrings) {
  const functions = filterStrings.map(parseFilter);
  return (record) => {
    for (let fn of functions) { if (fn(record)) { return true; } }
    return false;
  };
}

function parseFilter(filterString) {
  if (!filterString.includes(";") && !filterString.includes("return")) { filterString = "return " + filterString; }
  const levelNames = [];
  const levelValues = [];
  for (let levelName of LEVELS) {
    levelNames.push(levelName.toUpperCase());
    levelValues.push(LEVEL_MAP[levelName]);
  }
  return new Function(...levelNames, "log", filterString)
      .bind(null, ...levelValues);
}
